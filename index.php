<?php
// Récupération des données du formulaire
$nom = $_POST['nom'];

// Connexion à la base de données
$servername = "localhost";
$username = "nom_utilisateur";
$password = "mot_de_passe";
$dbname = "nom_base_de_donnees";

$conn = new mysqli($servername, $username, $password, $dbname);

// Vérification de la connexion
if ($conn->connect_error) {
    die("Connexion échouée : " . $conn->connect_error);
}

// Préparation et exécution de la requête d'insertion
$sql = "INSERT INTO table_nom (nom) VALUES ('$nom')";

if ($conn->query($sql) === TRUE) {
    echo "Enregistrement réussi";
} else {
    echo "Erreur lors de l'enregistrement : " . $conn->error;
}

// Fermeture de la connexion
$conn->close();
?>